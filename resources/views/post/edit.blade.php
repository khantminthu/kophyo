@extends('layouts.template')

@section('content')

<div class="col-md-8">
	@if(count($errors))
	<div class="alert alert-danger">
		<ul>
		@foreach($errors->all() as $error)
		<li>{{ $errors}}</li>
		@endforeach
		</ul>
	</div>
	@endif
	<form method="post" action="/post/update" enctype="multipart/form-data" class="my-5">
		@csrf
		<input type="hidden" name="postid"value="{{$post->id}}">
		<div class="form-group">
			<label class="form-control-label">Post title:</label>
			<input type="text" name="title" class="form-control"value="{{$post->title}}">
		</div>
		<div class="form-group">
			<label class="form-control-label"> Category</label>

			<select name="category" class="form-control" >
				@foreach($categories as $category)
				<option value="{{ $category->id }}"@if($post->category_id==$category->id)
				{{'selected'}}
					@endif>{{ $category->category_name }}</option>
				
				
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label class="form-control-label">Photo:</label>
			<input type="file" name="photo" class="form-control-file">
		</div>
		<div class="form-group">
			<img src="{{$post->photo}}"width="200"height="200">
		</div>
		<!-- <div class="form-group" ></div> -->
		<div class="form-group">
			<label class="form-control-label">Post Body:</label>
			<textarea name="body" class="form-control" id="summernote">{{ $post->body}}</textarea>
		</div>
		<div class="form-group">
			<input type="submit" name="update" class="btn btn-primary" value="update">
		</div>
	</form>
</div>

@endsection