 


=========blog.blade.php=============
 @foreach($posts as $post)


  <!-- Blog Post -->
  <div class="card mb-4">
    <img class="card-img-top" src="{{ $post->photo }}" alt="Card image cap">
    <div class="card-body">
      <h2 class="card-title">{{ $post->title }}</h2>
      <p class="card-text">{{str_limit(strip_tags($post->body),50)}}</p>
      <a href="/post/{{ $post->id }}" class="btn btn-primary">Read More &rarr;</a>
    </div>
    <div class="card-footer text-muted">
      Posted on {{ $post->created_at->diffForHumans() }} by
      <a href="#">{{$post->user->name}}</a>
    </div>
  </div>
  @endforeach

  <!-- Pagination -->
  <ul class="pagination justify-content-center mb-4">
    <li class="page-item">
      <a class="page-link" href="#">&larr; Older</a>
    </li>
    <li class="page-item disabled">
      <a class="page-link" href="#">Newer &rarr;</a>
    </li>
  </ul>
========================end








 ==================== templat================
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
  <script>
      $('#summernote').summernote({
        placeholder: 'Hello bootstrap 4',
        tabsize: 2,
        height: 100
      });
    </script>
    ===================end================

    =====================post/creatt.blade.php==========
    @if(count($errors))
  <div class="alert alert-danger">
    <ul>
    @foreach($errors->all() as $error)
    <li>{{ $errors}}</li>
    @endforeach
    </ul>
  </div>
  @endif
  <form method="post" action="/add" enctype="multipart/form-data" class="my-5">
    @csrf
    <div class="form-group">
      <label class="form-control-label">Post title:</label>
      <input type="text" name="title" class="form-control">
    </div>
    <div class="form-group">
      <label class="form-control-label"> Category</label>

      <select name="category" class="form-control" >
        @foreach($categories as $category)
        <option value="{{ $category->id }}">{{ $category->category_name }}</option>
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <label class="form-control-label">Photo:</label>
      <input type="file" name="photo" class="form-control-file">
    </div>
    <!-- <div class="form-group" ></div> -->
    <div class="form-group">
      <label class="form-control-label">Post Body:</label>
      <textarea name="body" class="form-control" id="summernote"></textarea>
    </div>
    <div class="form-group">
      <input type="submit" name="btnsubmit" class="btn btn-primary" value="Upload">
    </div>
  </form>
  ========================================