@extends('layouts.template')

@section('content')

 <div class="col-md-8">

 	<h2>Category List</h2>
 	
 	<!-- like wit echo -->
 	
	<a href="/add_category" class="btn btn-primary ml-auto mb-1"> Add New Category</a>

 	<table class="table">
 		<thead>
 			<tr>
 				<th>No.</th>
 				<th>Category Name</th>
 				<th>Edit</th>
 				<th>Delete</th>
 			</tr>
 		</thead>
 		<tbody>
 			@foreach($categories as $category)
 			<tr>
 				<td>{{ $loop->iteration }}</td>
 				<td>{{ $category->category_name}}</td>
 				<td><a href="/category/edit/{{$category->id}}" class="btn btn-warning">Edit</a></td>
 				<td><a href="/category/delete/{{$category->id}}" class="btn btn-danger">Delete</a></td>
 			</tr>

 			@endforeach
 		</tbody>
 	</table>
 </div>

@endsection
