@extends('layouts.template')

@section('content')

	<div class="col-md-8">
		<h2>Edit Category</h2>
		<form action="/category/update" method="post">
			@csrf
			<div class="form-group">
				<input type="hidden" name="cat_id"value="{{$category->id}}">
				<label>Category Name</label>
				<input type="text" name="category" class="form_control" value="{{$category->category_name}}">
			</div>
			<div class="form-group">
				<input type="submit" name="update"value="Update"class="btn btn-primary">
			</div>
		</form>
	</div>

@endsection