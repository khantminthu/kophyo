@extends('layouts.template')

@section('content')

	<div class="col-md-8">
		<h2>Add New Category</h2>
		<form action="/save_category" method="post">
			@csrf
			<div class="form-group">
			<label>Category Name</label>
			<input type="text" name="name" class="form-control">
			</div>
			<div class="form-group">
				<input type="submit" name="btnsubmit" value="Add" class="form-control">
			</div>
		</form>
	</div>

@endsection